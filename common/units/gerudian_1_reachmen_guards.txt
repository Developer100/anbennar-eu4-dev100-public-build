#

type = infantry
unit_type = tech_gerudian

maneuver = 1
offensive_morale = 0
defensive_morale = 1
offensive_fire = 0
defensive_fire = 0
offensive_shock = 1
defensive_shock = 1

trigger = {
	NOT = {
		primary_culture = grombar_half_orc
		primary_culture = grombar_orc
		primary_culture = gray_orc
	}
}