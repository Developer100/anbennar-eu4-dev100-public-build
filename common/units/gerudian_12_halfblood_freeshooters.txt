type = infantry
unit_type = tech_gerudian

maneuver = 1
offensive_morale = 2
defensive_morale = 2
offensive_fire = 1
defensive_fire = 0
offensive_shock = 3
defensive_shock = 1

trigger = {
	OR = {
		primary_culture = grombar_half_orc
		primary_culture = grombar_orc
		primary_culture = gray_orc
	}
}