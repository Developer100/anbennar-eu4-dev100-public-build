# Western Medieval Knights

type = cavalry
unit_type = tech_gerudian

maneuver = 2
offensive_morale = 1
defensive_morale = 1
offensive_fire = 1
defensive_fire = 1
offensive_shock = 1
defensive_shock = 2

trigger = {
	NOT = {
		primary_culture = grombar_half_orc
		primary_culture = grombar_orc
		primary_culture = gray_orc
	}
}
