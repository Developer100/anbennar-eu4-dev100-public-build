type = infantry
unit_type = tech_gerudian

maneuver = 1
offensive_morale = 4
defensive_morale = 3
offensive_fire = 3
defensive_fire = 3
offensive_shock = 3
defensive_shock = 4

trigger = {
	OR = {
		primary_culture = grombar_half_orc
		primary_culture = grombar_orc
		primary_culture = gray_orc
	}
}