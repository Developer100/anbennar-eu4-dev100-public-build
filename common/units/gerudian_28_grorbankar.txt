# Western Medieval Knights

type = cavalry
unit_type = tech_gerudian

maneuver = 2
offensive_morale = 4
defensive_morale = 3
offensive_fire = 2
defensive_fire = 3
offensive_shock = 5
defensive_shock = 3

trigger = {
	OR = {
		primary_culture = grombar_half_orc
		primary_culture = grombar_orc
		primary_culture = gray_orc
	}
}